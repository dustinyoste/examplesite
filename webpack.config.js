const path = require('path');

const FlowWebpackPlugin = require('flow-webpack-plugin')

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './dist/index.html',
    filename: 'index.html',
    inject: 'body'
})

module.exports = {
    resolve: {
        modules: [path.resolve(__dirname, './src'), 'node_modules'],
        extensions: ['.js', '.jsx', '.json']
    }, 
    entry: './dist/index.js',
    output: {
        path: path.resolve('dist'),
        filename: 'bundle.js'
    },
    devServer: {
        historyApiFallback: true,
        port: 8080,
    },
    plugins: [HtmlWebpackPluginConfig, new FlowWebpackPlugin()],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            },
            {
                test: /\.svg$/,
                use: 'react-svg-loader'
            },

            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    { loader: 'babel-loader' },
                    { loader: 'eslint-loader' },
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    { loader: 'file-loader', options: {} }
                ]
            },
        ]
    }
};
