//@flow

import React, { Component } from 'react';
import styled from 'styled-components';

import Button from 'styles/button';
import LogoIcon from 'components/icons/logo';
import { Homefont } from 'styles';

type Props = {
    className?: string,
}

export default class Footer extends Component<Props> {
    render() {
        const { className } = this.props;

        return (
            <Container className={ className }>
                <LogoIcon/>
                <Headline>Egestas congue quisque</Headline>
                <SubText>Fames ac turpis egestas sed tempus urna et pharetra.</SubText>
                <GetInTouchButton small onClick={ () => {} }>Varius duis at</GetInTouchButton>
            </Container>
        );
    }
}

const Container = styled.div`
    height: 50vh;

    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: #fff;

    font-family: ${ Homefont };
`;

const Headline = styled.div`
    text-align: center;
    font-size: 2rem;
    font-weight: 300;
    margin-top: 1rem;
`;

const SubText = styled.div`
    font-weight: 300;
    margin-top: 2.5rem;
    text-align: center;
    font-size: 1.5rem;
`;

const GetInTouchButton: typeof(Button) = styled(Button)`
    margin-top: 3rem;
    align-self: center;
`;
