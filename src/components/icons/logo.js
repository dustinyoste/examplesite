//@flow

import React, { Component } from 'react';

export default class Logo extends Component<{}> {
    render() {
        return (
            <svg width="38" height="32" viewBox="0 0 38 32" xmlns="http://www.w3.org/2000/svg" { ...this.props }><title>abc</title><g fill="none" fillRule="evenodd"><path className="logo-top" d="M7.52 15.042c4.156 0 7.522-3.37 7.522-7.522 0-4.15-3.366-7.52-7.52-7.52C3.366 0 0 3.37 0 7.52c0 4.152 3.367 7.522 7.52 7.522M24.938 15.042c4.155 0 7.52-3.37 7.52-7.522 0-4.15-3.365-7.52-7.52-7.52-4.155 0-7.52 3.37-7.52 7.52 0 4.152 3.365 7.522 7.52 7.522" fill="#fff"></path><path className="logo-bottom" d="M30.48 31.667c4.153 0 7.52-3.37 7.52-7.523 0-4.15-3.367-7.52-7.52-7.52-4.156 0-7.522 3.37-7.522 7.52 0 4.153 3.366 7.523 7.52 7.523" fill="#f2d314"></path></g>
            </svg>
        );
    }
}


