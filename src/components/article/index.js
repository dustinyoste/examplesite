//@flow

import React, { Component, type Node } from 'react';
import styled from 'styled-components';

type Props = {
    img: string,
    headline: string,
    content: Node,
    className?: string,
};

export default class Article extends Component<Props> {
    render() {
        const { img, headline, content, className } = this.props;

        return (
            <Container className={ className }>
                <img src={ img }/>
                <Headline>{ headline }</Headline>
                <Content>{ content }</Content>
            </Container>
        );
    }
}

const Container = styled.div`
    > img {
        height: 16rem;
        margin: 2rem;
        object-fit: contain;
    }
`;

const Headline = styled.div`
    text-transform: uppercase;
    font-weight: 600;
    margin-bottom: 1rem;
    font-size: 0.85rem;
`;

const Content = styled.div`
    line-height: 30px;
    margin-right: 2rem;
`;

