//@flow

import React, { Component } from 'react';
import styled from 'styled-components';

type Props = {
    img: string,
    name: string,
    desc: string,
    className?: string,
};

export default class Banner extends Component<Props> {
    render() {
        const { img, name, desc, className } = this.props;

        return (
            <Container className={ className }>
                <img src={ img }/>
                <Overlay className={ 'overlay' }/>
                <Text className={ 'text' }>
                    <Name>{ name }</Name>
                    <Desc>{ desc }</Desc>
                </Text>
            </Container>
        );
    }
}

const Container = styled.div`
    cursor: pointer;

    position: relative;
    display: flex;

    height: 40vw;
    width: 100%;

    color: #fff;

    > * {
        position: absolute;
    }

    > img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    &:hover {
        .overlay {
            opacity: 0.39;
        }
    }
`;

const Text = styled.div`
    font-size: 2.7rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    width: 50%;
`;

const Name = styled.div`
    font-weight: 600;
    margin-bottom: 1rem;
`;

const Desc = styled.div`
`;

const Overlay = styled.div`
    width: 100%;
    height: 100%;

    background-color: rgb(45,51,55);
    transition: 0.5s;

    opacity: 0.24;
`;
