//@flow

import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import styled from 'styled-components';

import { Provider } from 'react-redux';
import store from 'store';

import { List } from 'immutable';

import Home from 'components/pages/home';
import Work from 'components/pages/work';
import Services from 'components/pages/services';
import About from 'components/pages/about';
import Garage from 'components/pages/garage';
import Contact from 'components/pages/contact';
import Footer from 'components/footer';

import NavBar from 'components/nav';
import Tab from 'components/nav/tab';


export default class App extends Component<{}, {
    scrollingUp: boolean,
    scrollOffset: number,
    tabs: List<any>,
}> {
    constructor() {
        super();

        this.state = {
            scrollingUp: false,
            scrollOffset: 0,
            tabs: List([
                new Tab({ name: 'Work', url: 'work' }),
                new Tab({ name: 'Services', url: 'services' }),
                new Tab({ name: 'About', url: 'about' }),
                new Tab({ name: 'Garage', url: 'garage' }),
                new Tab({ name: 'Contact', url: 'contact' }),
            ]),
        };
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = (event: SyntheticInputEvent<>) => {
        const { scrollOffset } = this.state;
        // $FlowFixMe
        const currentOffset = event.target.scrollingElement.scrollTop;

        this.setState({
            scrollOffset: currentOffset,
            scrollingUp: currentOffset < scrollOffset && currentOffset !== 0,
        });
    }

    render() {
        const { scrollOffset, scrollingUp, tabs } = this.state;
        
        return (
            <Provider store={ store }>
                <Router>
                    <Content>
                        <Route exact path="/" component={ Home }/>
                        <Route exact path="/work" component={ Work }/>
                        <Route exact path="/services" component={ Services }/>
                        <Route exact path="/about" component={ About }/>
                        <Route exact path="/garage" component={ Garage }/>
                        <Route exact path="/contact" component={ Contact }/>
                        <Nav scroll={ scrollOffset } appearing={ scrollingUp } tabs={ tabs }/>
                        <Footer/>
                    </Content>
                </Router>
            </Provider>
        );
    }
}

const Nav: typeof NavBar = styled(NavBar)`
    position: fixed;
    transition: 500ms;
    ${ props => props.appearing || props.scroll === 0 ? `top: 0` : `top: -6rem` };
    left: 0;
    right: 0;
    padding: 1rem 4rem;
`;

const Content = styled.div`
`;
