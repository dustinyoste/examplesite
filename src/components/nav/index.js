//@flow

import React, { Component } from 'react';
import { withRouter } from 'react-router';
import styled from 'styled-components';

import { List } from 'immutable';

import Tab from 'components/nav/tab';

import { Defaultfont, DarkText } from 'styles';
import LogoIcon from 'components/icons/logo';

type Props = {
    tabs: List<Tab>,
    appearing: boolean,
    className?: String,
    history: any,
    location: any,
};

@withRouter
export default class Nav extends Component<Props> {
    static defaultProps = {
        history: null,
        location: null,
    }

    render() {
        const { tabs, appearing, history, location, className } = this.props;
    
        return (
            <Container appearing={ appearing } className={ className }>
                <Logo onClick={ () => history.push('/') }/>
                <Tabs>
                    {
                        tabs.map(t => (
                            <TabNav selected={ location.pathname.includes(t.url) } onClick={ () => history.push(t.url) } key={ t.name }>
                                { t.name }
                            </TabNav>
                            )
                        )
                    }
                </Tabs>
            </Container>
        );
    }
}

const Container = styled.div`
    display: flex;
    justify-content: space-between;

    transition: 500ms;
    ${ props => props.appearing ? `
        background: rgba(255,255,255,0.94);
        color: ${ DarkText };

        > svg {
            .logo-top {
                transition: 500ms;
                fill: ${ DarkText };
            }
        }
    ` : `
        color: #fff;
    ` };

    > * {
        margin-right: 1rem;
    }

    font-family: ${ Defaultfont };
`;

const Tabs = styled.div`
    display: flex;
    align-items: center;
`;

const Logo = styled(LogoIcon)`
    cursor: pointer;
`;

const TabNav  = styled.div`
    text-transform: uppercase;
    font-weight: 600;
    font-size: 0.857rem;
    position: relative;
    cursor: pointer;
    margin: 0 2rem;

    &:before {
        content: '';
        position: absolute;
        top: 0;
        bottom: 0;
        margin: auto;
        left: -10px;
        width: 5px;
        height: 5px;
        border-radius: 50%;
        background-color: #F3D311;
        transition: opacity .15s;
        opacity: ${ props => props.selected ? `1` : `0` };
    }

    &:hover:before {
        opacity: 1;
    }
`;
