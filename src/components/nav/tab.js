import { Record } from 'immutable';

export default class Tab extends Record ({
    name: String,
    url: String,
}) {
    get name() { return this.get('name') }
    get url() { return this.get('url') }
}
