//@flow

import React, { Component } from 'react';
import styled from 'styled-components';

import { List } from 'immutable';

import Hero from 'components/pages/home/hero';
import About from 'components/pages/home/about';
import About2 from 'components/pages/home/about_2';
import Banner from 'components/banner';
import Articles from 'components/pages/home/articles';
import About3 from 'components/pages/home/about_3';

import { Homefont } from 'styles';

export default class Home extends Component<{}> {
    render() {
        const placeholderImg = 'https://via.placeholder.com/283x696.jpg';
        const banners = List([
            { img: placeholderImg, name: 'Integer', desc: 'Etiam non quam lacus suspendisse faucibus' },
            { img: placeholderImg, name: 'Mal Esuada', desc: 'Blandit aliquam etiam erat velit scelerisque' },
            { img: placeholderImg, name: 'SITamet', desc: 'Mi ipsum faucibus vitae aliquet nec' },
        ]);

        return (
            <Container>
                <Hero/>
                <About/>
                <Banners>
                {
                    banners.map(b => (
                        <Banner key={ b.name } img={ b.img } name={ b.name } desc={ b.desc }/>
                    ))
                }
                </Banners>
                <About2/>
                <Articles/>
                <About3/>
            </Container>
        );
    }
}

const Container = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    background: #fff;

    font-family: ${ Homefont };

    border-top: solid 2px #f3d311;
    border-bottom: solid 2px #f3d311;
`;

const Banners = styled.div`
    margin: 2rem;

    display: flex;
    flex-direction: column;

    > * {
        margin-bottom: 1rem;

        &:nth-child(odd) {
            .text {
                left: 7.85rem;
            }
        }
        &:nth-child(even) {
            .text {
                right: 2rem;
            }
        }
    }
`;
