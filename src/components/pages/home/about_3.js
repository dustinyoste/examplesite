//@flow

import React, { Component } from 'react';
import styled from 'styled-components';

import { LightText } from 'styles';
import Button from 'styles/button';

export default class About3 extends Component<{}> {
    render() {
        return (
            <Container>
                <img src={ 'https://via.placeholder.com/371x720.jpg' }/>
                <Content>
                    <Headline>Placerat duis ultricies lacus sed turpis tincidunt id aliquet</Headline>
                    <SubText>Scelerisque eu ultrices vitae auctor eu augue ut lectus arcu bibendum at varius vel pharetra vel turpis nunc eget lorem dolor sed viverra.</SubText>
                    <GetToKnowButton onClick={ () => {} }>Morbi tincidunt augue</GetToKnowButton>
                </Content>
            </Container>
        );
    }
}

const Container = styled.div`
    position: relative;

    color: ${ LightText };

    display: flex;
    align-items: center;
    justify-content: center;

    > img {
        position: absolute;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
`;

const Content = styled.div`
    z-index: 0;
    margin: 8rem;
    max-width: 65%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

const Headline = styled.div`
    text-align: center;
    font-size: 3.85rem;
    font-weight: 600;
`;

const SubText = styled.div`
    font-size: 1.7rem;
    font-weight: 300;
    align-self: flex-start;
    margin-top: 2.5rem;
    text-align: center;
`;

const GetToKnowButton: typeof(Button) = styled(Button)`
    margin-top: 3rem;
    align-self: center;
`;
