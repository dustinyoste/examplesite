//@flow

import React, { Component } from 'react';
import styled from 'styled-components';
import { DarkText } from 'styles';

export default class About extends Component<{}> {
    render() {
        return (
            <Container>
                <span>Eleifend mi in nulla posuere</span>
                <Divider/>
                <span>Arcu dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis.</span>
            </Container>
        );
    }
}

const Container = styled.div`
    background: #fff;
    color: ${ DarkText };
   
    margin: 6rem;
    display: flex;
    flex-direction: column;

    align-items: center;
    text-align: center;
    
    > * {
        &:not(last-child) {
            margin-bottom: 2rem;
        }
    }

    > span {
        &:first-child {
            font-size: 5.33rem;
            font-weight: 600;
        }

        &:last-child {
            font-size: 1.7rem;
            font-weight: 300;
        }
    }
`;

const Divider = styled.div`
    margin: 1rem;
    width: 5rem;
    height: 0.5rem;
    background: #F3D311;
`;
