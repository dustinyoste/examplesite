//@flow

import React, { Component } from 'react';
import styled from 'styled-components';

import { connect } from 'react-redux';

import { List } from 'immutable';

import Article from 'components/article';

import { GreyBG } from 'styles';
import Button from 'styles/button';


type Props = {
    dispatch: any,
    images: any,
};

@connect(st => ({ images: st.images }), d => ({ dispatch: d }))
export default class Articles extends Component<Props, {
    image: any,
}> {
    static defaultProps = {
        dispatch: null,
        images: null,
    }

    constructor(props: Props) {
        super(props);
        
        props.dispatch({ type: 'images', call: 'get', id: 1 });
        
        this.state = {
            image: props.images.get(1),
        }
    }

    render() {
        const { images } = this.props;

        if (!images.get(1) || !images.get(1).get('value')) {
            return null;
        }

        images.get(1).get('value').then(v => console.log(v));

        const articles = List([
            {
                img: 'https://via.placeholder.com/239x253.jpg',
                headline: '001. Posuere',
                content: <div>Suscipit tellus mauris a diam maecenas sed enim ut sem viverra aliquet eget sit amet tellus cras.</div>,
            },
            {
                img: 'https://via.placeholder.com/239x253.jpg',
                headline: '002. Faucibus',
                content: <div>Nullam eget felis eget nunc lobortis mattis aliquam faucibus purus in massa tempor nec.</div>,
            },
            {
                img: 'https://via.placeholder.com/239x253.jpg',
                headline: '003. Elementum',
                content: <div>Malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat.</div>,
            },
        ]);
        
        return (
            <Container>
                <ArticleList>
                    {
                        articles.map(a => (
                            <Article key={ a.headline } img={ a.img } headline={ a.headline } content={ a.content }/>
                        ))
                    }
                </ArticleList>
                <LearnButton onClick={ () => {} } filled>sit amet facilisis</LearnButton>
            </Container>
        );
    }
}

const Container = styled.div`
    background: ${ GreyBG };
    display: flex;
    flex-direction: column;
    padding-bottom: 6rem
`;

const ArticleList = styled.div`
    width: 100%;
    justify-content: center;
    display: flex;

    > * {
        width: 30%;

        > img {
            width: 60%;
        }
    }
`;

const LearnButton: typeof(Button) = styled(Button)`
    margin-top: 3rem;
    align-self: center;
`;
