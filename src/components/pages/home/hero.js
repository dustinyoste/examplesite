//@flow

import React, { Component } from 'react';
import styled from 'styled-components';

export default class Hero extends Component<{}> {
    render() {
        return (
            <Container>
                <img src={ 'https://via.placeholder.com/225x400.jpg' }/>
                <Overlay/>
                <Text>{ `Feugiat,\nsed lectus` }</Text>
            </Container>
        );
    }
}

const Container = styled.div`
    display: flex;
    height: 100vh;
    justify-content: center;
    align-items: center;
    
    > * {
        position: absolute;
    }

    > img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
`;

const Overlay = styled.div`
    width: 100%;
    height: 100%;
    background: rgba(45,51,55,0.49);
`;

const Text = styled.div`
    white-space: pre;
    position: absolute;
    color: #fff;
    font-weight: 600;
    font-size: 6.6rem;
    margin: 2rem;
    left: 10%;
`;
