//@flow

import React, { Component } from 'react';
import styled from 'styled-components';

import { DarkText, GreyBG } from 'styles';

export default class About2 extends Component<{}> {
    render() {
        return (
            <Container>
                <div>
                    <Headline>Sed cras ornare arcu dui vivamus</Headline>
                    <Divider/>
                </div>
                <SubText>Bibendum at varius vel pharetra vel turpis nunc eget lorem dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida.</SubText>
            </Container>
        );
    }
}

const Container = styled.div`
    background: ${ GreyBG };
    color: ${ DarkText };

    display: flex;
    align-items: center;
    justify-content: center;

    padding: 2rem 3rem;
    padding-top: 6rem;
    margin-top: -6rem;

    > * {
        width: 45%;
        
        &:first-child {
            margin-right: 1rem;
        }
    }
`;

const SubText = styled.div`
    font-size: 1.7rem;
    font-weight: 300;
    align-self: flex-start;
    margin-top: 2.5rem;
`;

const Headline = styled.div`
    font-size: 3.85rem;
    font-weight: 600;
`;

const Divider = styled.div`
    margin-top: 2rem;
    width: 5rem;
    height: 0.5rem;
    background: #F3D311;
`;
