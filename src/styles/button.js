//@flow

import React, { Component, type Node } from 'react';
import styled from 'styled-components';

import { TTYellow } from 'styles';

type Props = {
    filled?: boolean,
    small?: boolean,
    children?: Node,
    onClick: () => void,
    className?: string,
};

export default class Button extends Component<Props> {
    render() {
        const { onClick, filled, small, children, className } = this.props;

        return (
            <Container onClick={ onClick } small={ small } filled={ filled } className={ className }>
                { children }
                <Overlay filled={ filled } className={ 'overlay' }/>
            </Container>
        );
    }
}

const Container = styled.div`
    position: relative;
    width: ${ props => props.small ? `15rem` : `20rem` };
    font-size: ${ props => props.small ? `0.8rem` : `1.14rem` };

    border: ${ props => !props.filled ? `solid 1px rgba(255,255,255, 0.5)` : `` };
    background: ${ props => props.filled ? TTYellow :  `` };

    cursor: pointer;

    display: flex;
    justify-content: center;
    align-items: center;

    border-radius: 2px;
    font-family: "Avenir", "Avenir LT W01 65 Medium", sans-serif;
    color: #fff;
    text-transform: uppercase;
    height: 4.6rem;
    font-weight: 800;
    letter-spacing: 2px;

    &:hover {
        .overlay {
            opacity: 0.14;
        }
    }
`;

const Overlay = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
     
    background-color: ${ props => props.filled ? `rgb(45,51,55)` : `#fff` };
    transition: 0.5s; 
    
    opacity: 0;
`;

