// @flow

export const DarkText = '#2D3337';
export const LightText = '#fff';

export const GreyBG = '#F1F1F1';

export const Defaultfont = `"Avenir", "Avenir LT W01 65 Medium", sans-serif`;
export const Homefont = `"MercurySSm", serif`;

export const TTYellow = '#F3D210';
