//@flow

export function getImage(id: number) {
    return fetch(`http://127.0.0.1:3000/images/${ id }`, {
        method: "GET",
    }).then(response => {
        return response.json();
    }).catch(error => {
        console.log(error);
        return null;
    });
}

