//@flow

import { combineReducers, createStore } from 'redux'
import { getImage } from 'api/images';
import { Map } from 'immutable';

function images(state = Map({}), action: any) {
    if (action.type !== 'images') {
        return state;
    }

    if (action.call === 'get') {
        const image = getImage(action.id);
        
        const v = state.get(action.id) || new Map();
        
        const x = v.set('loading', 'true').set('value', image);
        return state.set(action.id, x);
    }

    return state;
}

const reducers = combineReducers({
    images,
})

export default createStore(reducers)
